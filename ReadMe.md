﻿# Testen ~ Kristof Smits
De repo can gevonden worden op: https://gitlab.com/_Kristof_/kristof-carsurancecalculatorunittest/

De 'main' branch bevat de unit test met nog onverbeterde code, de verbeterde code is te finden in de  'WithFixes' branch.

## Is License Age Calculated Correctly

### Doel:
Bevestigen dat de License Age correct berekent wordt.

### Technieken:

 - Fact

### Dataset:

 - Leeftijd:  22
 - Rijbewijs start datum: nu - 5 jaar

### Verwachting:
Een License Age van 5 jaar

### Resultaat:

Een License Age van 5 jaar, dus de berekening klopt.

## Is Base Premium Calculated Correctly

### Doel:
Bevestigen dat de Basis Premie correct berekent word volgens de formule in de requirements: 
(voertuig / 100 - leeftijd + vermogen in KW / 5) / 3

### Technieken:

 - Fact

### Dataset:

 - Leeftijd: 5 
 - Vermogen in KW: 20
 - Prijs: 1000

### Verwachting:
Dat de uitkomst 3 is want:
(1000 / 100 - 5 + 20 / 5) / 3 = 3

### Resultaat:

Een incorrecte uitkomst van 11.
Dit kwam doordat de haakjes vergeten waren in de berekening.

## Is 15 Percent Increase Correctly Applied

### Doel:
Bevestigen van requirement:
- Een leeftijd van onder 23 jaar OF korter dan 5j rijbewijs is premie-opslag van 15%

### Technieken:

 - Theory

### Dataset:
Er is gebruik gemaakt van de grenswaarden: Leeftijd - Rijbewijs 
 - 23 - 10 (Verwachting: geen opslag)
 - 40 - 5 (Verwachting: geen opslag)
 - 22 - 10 (Verwachting: opslag)
 - 40 - 4 (Verwachting: opslag)

### Resultaat:
De test met dataset 40 - 5 had opslag terwijl die geen opslag hoorde te krijgen.
Dit kwam doordat er '<= 5' stond in plaats van '< 5'.

## Is Postal Code Increase Correctly Applied

### Doel:
Bevestigen dat opslag van postcodes goed word toegepast:
- Postcodes 10xx - 35xx: 5% risico opslag - Postcodes 36xx - 44xx: 2% risico opslag

### Technieken:

 - Theory

### Dataset:
Er is gebruik gemaakt van de grenswaarden: postcode - verwachte multiplier

 - 1000 - 1.05
 - 3599 - 1.05
 - 3600 - 1.02
  - 4499 - 1.02
 - 5500 - 1

### Resultaat:
De opslag voor post codes werd correct toegepast. 

## Is Insurance Coverage Increase Applied Correctly

### Doel:
Bevestigen dat het type insurance coverage correct de prijs verhoogt:
- WA plus is 20% duurder 
- All Risk is dubbele premie (van WA)

### Technieken:

 - Theory

### Dataset:
Het type coverage - der verwachte multiplier op WA.
- WA plus - 1.2
- All Risk - 2

### Resultaat:
De prijs werd correct verhoogt.


## Is Payment In Year Discount Applied Correctly

### Doel:
Bevestigen dat je de juiste korting krijgt als je per jaar betaalt:

Premie per jaar / 12 is maandpremie. Voor jaarbetaling geldt 2,5% korting

### Technieken:

 - Fact
 - Mocken

### Dataset:

- Premium per jaar: 1000

### Verwachting:
97.5% van 1000 dus 975

### Resultaat:
De korting was niet goed berekend, er kwam 975.61 uit in plaats van 975.
Er was gebruik gemaakt van / 1.025 maar dit moet zijn * 0.975

## Is No Claim Discount Applied Correctly

### Doel:
Bevestigen dat de no claim korting goed toegepast wordt:
Voor 6e schadevrije jaar en daarboven wordt 5% korting gerekend (5,10,15,20) met een maximum van 65%.


### Technieken:

 - Theory

### Dataset:
Er is gebruik gemaakt van de grenswaarden (en één waarde die zorgt voor rekenen met negatieve getallen):
Aantal No Claim jaren - Verwachte multiplier

 - 0 - 1
 - 5 - 1
 - 6 - 0.95
  - 18 - 0.35
 - 19 - 0.35

### Resultaat:
De zelf brekende uitkomst met de multiplier komt niet overeen met het resultaat.
Dit kwam doordat er een int was gebruikt in plaats van een double wat zorgde voor fraction loss.

## Is Negative License Age Decked

### Doel:
Bevestigen dat de license age niet negatief word als je drivers license startdatum nog niet geweest is.


### Technieken:

 - Fact

### Dataset:

- Driver License Start Date: 1 jaar in de toekomst

### Verwachting:
Dat license age 0 is.

### Resultaat:
De license age was -1.
