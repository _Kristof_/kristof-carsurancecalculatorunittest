﻿namespace WindesheimAD2021AutoVerzekeringsPremieXUnitTest
{
    using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Xunit;

    public class PolicyHolderTest
    {
        [Fact]
        public void IsLicenseAgeCalculatedCorrectly()
        {
            //setup
            const int age = 22;
            const int ExpectedLicenseAge = 5;
            DateTime DriverLicenseStartDate = DateTime.Now.AddYears(- ExpectedLicenseAge);

            PolicyHolder policyHolder = new(age, DriverLicenseStartDate.ToString("dd-MM-yyyy"), 5555, 1);

            //compare
            Assert.Equal(ExpectedLicenseAge, policyHolder.LicenseAge);
        }

        [Fact]
        public void IsNegativeLicenseAgeDecked()
        {
            //setup
            PolicyHolder policyHolder = new(18, DateTime.Now.AddYears(1).ToString("dd-MM-yyyy"), 555, 0);
            
            //compare
            Assert.Equal(0, policyHolder.LicenseAge);
        }
    }
}
