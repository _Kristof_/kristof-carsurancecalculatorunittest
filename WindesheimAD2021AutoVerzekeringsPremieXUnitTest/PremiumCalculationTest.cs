﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Moq;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremieXUnitTest
{
    public class PremiumCalculationTest
    {
        
        [Fact]
        public void IsBasePremiumCalculatedCorrectly()
        {
            //setup
            const int VehiclePrice = 1000;
            const int VehicleKW = 20;
            const int VehicleAge = 5;

            Vehicle vehicle = new(
                ValueInEuros: VehiclePrice,
                PowerInKw: VehicleKW,
                constructionYear: DateTime.Now.Year - VehicleAge
                );

            //calulcate
            //(voertuig / 100 - leeftijd + vermogen in KW / 5) / 3
            //(1000 / 100 - 5 + 20 / 5) / 3
            const double ExpectedBasePremium = 3;

            double ActualResult = PremiumCalculation.CalculateBasePremium(vehicle);

            //compare
            Assert.Equal(ExpectedBasePremium, ActualResult);
        }

        [Theory]
        //No Increase Expected
        [InlineData(23, 10, false)]
        [InlineData(40, 5, false)]

        //Increase Expected
        [InlineData(22, 10, true)] // Driver younger than 23 years
        [InlineData(40, 4, true)] // License younger that 5 years
        public void Is15PercentIncreaseCorrectlyApplied(int Age, int LicenseAge, bool Is15PercentIncreaseExpected)
        {
            //setup
            Vehicle vehicle = new(
                ValueInEuros: 1000,
                PowerInKw: 20,
                constructionYear: DateTime.Now.Year + 5
            );

            PolicyHolder policyHolder = new(
                Age,
                DateTime.Now.AddYears(-LicenseAge).ToString("dd-MM-yyyy"),
                9999,
                5
            );

            //calculate
            double basePremium = PremiumCalculation.CalculateBasePremium(vehicle);
            
            double expectedPremium = basePremium;

            if (Is15PercentIncreaseExpected)
            {
                expectedPremium *= 1.15;
            }

            PremiumCalculation actualPremiumCalculation = new(vehicle, policyHolder, InsuranceCoverage.WA);

            //compare
            Assert.Equal(expectedPremium, actualPremiumCalculation.PremiumAmountPerYear);
        }

        [Theory]
        [InlineData(1000, 1.05)]
        [InlineData(3599, 1.05)]
        [InlineData(3600, 1.02)]
        [InlineData(4499, 1.02)]
        [InlineData(5500, 1)]
        public void IsPostalCodeIncreaseCorrectlyApplied(int PostalCode, double ExpectedModifier)
        {
            //setup
            Vehicle vehicle = new(
                ValueInEuros: 1000,
                PowerInKw: 20,
                constructionYear: DateTime.Now.Year + 5
            );

            PolicyHolder policyHolder = new(
                30,
                DateTime.Now.AddYears(-20).ToString("dd-MM-yyyy"),
                PostalCode,
                5
            );
            
            //calculate
            double basePremium = PremiumCalculation.CalculateBasePremium(vehicle);

            double expectedPremium = basePremium * ExpectedModifier;

            PremiumCalculation actualPremiumCalculation = new(vehicle, policyHolder, InsuranceCoverage.WA);

            //compare
            Assert.Equal(expectedPremium, actualPremiumCalculation.PremiumAmountPerYear);
        }

        [Theory]
        [InlineData(InsuranceCoverage.WA_PLUS, 1.2)]
        [InlineData(InsuranceCoverage.ALL_RISK, 2)]
        private void IsInsuranceCoverageIncreaseAppliedCorrectly(InsuranceCoverage coverage, double ExpectedMultiplier)
        {
            //setup
            Vehicle vehicle = new(
                ValueInEuros: 1000,
                PowerInKw: 20,
                constructionYear: DateTime.Now.Year + 5
            );

            PolicyHolder policyHolder = new(
                30,
                DateTime.Now.AddYears(-20).ToString("dd-MM-yyyy"),
                5555,
                0
            );

            //calcualte
            PremiumCalculation WA = new(vehicle, policyHolder, InsuranceCoverage.WA);

            double ExpectedResult = WA.PremiumAmountPerYear * ExpectedMultiplier;

            PremiumCalculation actualPremiumCalculation = new(vehicle, policyHolder, coverage);

            //compare
            Assert.Equal(ExpectedResult, actualPremiumCalculation.PremiumAmountPerYear);
        }

        [Fact]
        public void IsPaymentInYearDiscountAppliedCorrectly()
        {
            //create mock
            Mock<PremiumCalculation> premiumMock = new(

                new Vehicle(100, 5000, 2008),
                
                new PolicyHolder(22, DateTime.Now.AddYears(-5).ToString("dd-MM-yyyy"), 5555, 0),

                InsuranceCoverage.WA
                );

            //setup mock
            premiumMock.SetupAllProperties();
            premiumMock.SetupGet(p => p.PremiumAmountPerYear).Returns(1000);

            double ActualPremium = premiumMock.Object.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR);

            //2,5% discount meaning 1000 - 25 = 975
            Assert.Equal(975, ActualPremium);
        }

        [Theory]
        [InlineData(0, 1)]
        [InlineData(5, 1)]
        [InlineData(6, 0.95)]
        [InlineData(18, 0.35)]
        [InlineData(19, 0.35)]
        public void IsNoClaimDiscountAppliedCorrectly(int NoClaimYears, double ExpectedMultiplier)
        {
            //setup
            Vehicle vehicle = new(100, 5000, DateTime.Now.Year - 10);
            PolicyHolder policyHolder = new(30, DateTime.Now.AddYears(-10).ToString("dd-MM-yyyy"), 9999, NoClaimYears);

            //calculate
            double basePremium = PremiumCalculation.CalculateBasePremium(vehicle);
            double ExpectedPremium = basePremium * ExpectedMultiplier;

            PremiumCalculation actuPremiumCalculation = new (vehicle, policyHolder, InsuranceCoverage.WA);

            //compare
            Assert.Equal(ExpectedPremium, actuPremiumCalculation.PremiumAmountPerYear);
        }
    }
}
